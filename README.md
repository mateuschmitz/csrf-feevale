Trabalho CSRF(Cross-site request forgery)
=========================================

Dados de aplicações utilizadas para teste.


Wordpress|
---------|------------
Versão   | 
Link     | [Link](http://mateuschmitz.com.br/wordpress/) [Admin](http://mateuschmitz.com.br/wordpress/wp-login.php)
Usuário  | admin
Senha    | admin

Passo a Passo
-------------

1) Faça login no Wordpress com os dados acima.
2) Após isso acesse a página: http://mateuschmitz.com.br/wordpress/wp-content/themes/twentyfifteen/genericons/exploit.shtml
3) Verifique os usuários da aplicação e veja que foi incluído um novo usuário administrador
4) Acesse o arquivo http://feevalecsrf.esy.es/__users_and_pass.txt e veja todas as senhas já criadas
